package com.andrea.examen.upax.utils.constants;

public class ConstantsAPI {
    public static final String URL_BASE = "https://api.themoviedb.org/3/";
    public static final String ENDPOINT_POPULAR_MOVIES = "movie/popular";
    public static final String API_KEY = "Bearer eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiI1ZmI2YTlhMDc0ZGQwOGMyNGEwYTgyMjU5NWVmMmZiMiIsInN1YiI6IjVlMTJiNjM2ZDhhZjY3MDAxNGJlMTdjYyIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.eD8OXwHQNW2pd_Jc6gNQhmJ8esEBYxEATDJ4gXAhAVI";
    public static final String LANGUAGE_REQUEST = "es";
    public static final String URL_IMAGE= "https://image.tmdb.org/t/p/w500/";
}
