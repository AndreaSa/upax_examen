package com.andrea.examen.upax.ui;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.andrea.examen.upax.R;
import com.andrea.examen.upax.databinding.ActivityMainBinding;
import com.andrea.examen.upax.service.LocationService;

public class MainActivity extends AppCompatActivity {
    private static final int TAG_CODE_PERMISSION_LOCATION = 10;
    private ActivityMainBinding activityMainBinding;
    private final int LIST_MOVIES = 1;
    private final int MAP = 2;
    private final int HOME = 0;
    private Fragment fragment;
    private int fragmentPos = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityMainBinding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(activityMainBinding.getRoot());
        agregarFragmentos(HOME);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) ==
                PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) ==
                        PackageManager.PERMISSION_GRANTED) { startService(new Intent(this, LocationService.class));
        } else {
            ActivityCompat.requestPermissions(this, new String[]{
                            Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_COARSE_LOCATION},
                    TAG_CODE_PERMISSION_LOCATION);
        }
    }



    public void agregarFragmentos(int pos) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        switch (pos) {
            case LIST_MOVIES:
                fragment = new MovieListFragment();
                fragmentPos = LIST_MOVIES;
                break;

            case HOME:
                fragment = new HomeFragment();
                fragmentPos = HOME;
                break;
            case MAP:
                fragment = new MapsFragment();
                fragmentPos = MAP;
                break;


        }
        transaction.setCustomAnimations(R.anim.zoom_forward_in, R.anim.zoom_forward_out, R.anim.zoom_back_in, R.anim.zoom_back_out)
                .replace(R.id.container, fragment)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //startActivity(new Intent(this,MainActivity.class));
    }
}