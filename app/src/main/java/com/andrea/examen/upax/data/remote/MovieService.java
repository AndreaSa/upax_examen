package com.andrea.examen.upax.data.remote;

import com.andrea.examen.upax.data.entity.PopularResponse;
import com.andrea.examen.upax.utils.constants.ConstantsAPI;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

public interface MovieService {

    @GET(ConstantsAPI.ENDPOINT_POPULAR_MOVIES)
    Call<PopularResponse> getPopular(@Header("Authorization") String authorization, @Query("language") String language);

}
