package com.andrea.examen.upax.ui;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.andrea.examen.upax.data.entity.Movie;
import com.andrea.examen.upax.databinding.FragmentMovieListListBinding;
import com.andrea.examen.upax.viewmodel.MovieViewModel;

import java.util.ArrayList;
import java.util.List;

public class MovieListFragment extends Fragment {
    private MyMovieRecyclerViewAdapter myMovieRecyclerViewAdapter;
    private MovieViewModel movieViewModel;
    private List<Movie> movies;

    public MovieListFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        FragmentMovieListListBinding fragmentMovieListListBinding = FragmentMovieListListBinding.inflate(inflater, container, false);
        View view = fragmentMovieListListBinding.getRoot();
        fragmentMovieListListBinding.list.setLayoutManager(new LinearLayoutManager(view.getContext()));
        movieViewModel = ViewModelProviders.of(getActivity()).get(MovieViewModel.class);
        myMovieRecyclerViewAdapter = new MyMovieRecyclerViewAdapter(view.getContext());
        fragmentMovieListListBinding.list.setAdapter(myMovieRecyclerViewAdapter);

        getData(fragmentMovieListListBinding);
        myMovieRecyclerViewAdapter.setData(movies);
        fragmentMovieListListBinding.list.setAdapter(myMovieRecyclerViewAdapter);

        fragmentMovieListListBinding.swip.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                                                                   @Override
                                                                   public void onRefresh() {
                                                                       fragmentMovieListListBinding.swip.setRefreshing(true);
                                                                       getData(fragmentMovieListListBinding);
                                                                       fragmentMovieListListBinding.swip.setRefreshing(false);

                                                                   }
                                                               }
        );

        return view;
    }

    public void getData(FragmentMovieListListBinding fragmentMovieListListBinding) {
        movieViewModel.getListMovies().observe(getActivity(), new Observer<List<Movie>>() {
            @Override
            public void onChanged(List<Movie> moviesL) {
                movies = moviesL;
                if (movies.size() > 0) {
                    fragmentMovieListListBinding.list.setVisibility(View.VISIBLE);
                    fragmentMovieListListBinding.textViewEmpty.setVisibility(View.GONE);
                } else {
                    fragmentMovieListListBinding.list.setVisibility(View.GONE);
                    fragmentMovieListListBinding.textViewEmpty.setVisibility(View.VISIBLE);
                }
                myMovieRecyclerViewAdapter.setData(movies);
            }
        });
    }




}