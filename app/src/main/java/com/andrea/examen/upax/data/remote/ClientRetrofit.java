package com.andrea.examen.upax.data.remote;

import com.andrea.examen.upax.utils.constants.ConstantsAPI;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ClientRetrofit {

    public static OkHttpClient getClient() {
        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.addInterceptor(httpLoggingInterceptor);

        return builder.build();
    }

    public static Retrofit getRetrofit() {
        return new Retrofit.Builder().baseUrl(ConstantsAPI.URL_BASE)
                .client(getClient())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

}
