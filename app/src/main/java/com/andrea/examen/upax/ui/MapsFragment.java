package com.andrea.examen.upax.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.andrea.examen.upax.R;
import com.andrea.examen.upax.app.MyApp;
import com.andrea.examen.upax.utils.DeviceUtils;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.firestore.QueryDocumentSnapshot;

import java.util.ArrayList;
import java.util.List;

public class MapsFragment extends Fragment {

    private OnMapReadyCallback callback = googleMap -> {
        List<LatLng> array = new ArrayList<>();
        MyApp.getFirestoreDb().collection("Marks")
                .whereEqualTo("Device", DeviceUtils.getDeviceName())
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        for (QueryDocumentSnapshot document : task.getResult()) {
                            array.add(new LatLng(Double.parseDouble(String.valueOf(document.get("latitude"))), Double.parseDouble(String.valueOf(document.get("longitude")))));
                        }
                        for (LatLng latLng : array) {
                            googleMap.addMarker(new MarkerOptions().position(latLng));
                        }
                        googleMap.moveCamera(CameraUpdateFactory.newLatLng(array.get(0)));
                    }
                });
    };

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_maps, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        if (mapFragment != null) {
            mapFragment.getMapAsync(callback);
        }
    }
}