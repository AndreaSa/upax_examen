package com.andrea.examen.upax.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.andrea.examen.upax.R;
import com.andrea.examen.upax.app.MyApp;
import com.andrea.examen.upax.data.entity.Movie;
import com.andrea.examen.upax.utils.NetworkUtils;
import com.andrea.examen.upax.utils.constants.ConstantsAPI;
import com.bumptech.glide.Glide;

import java.util.List;


public class MyMovieRecyclerViewAdapter extends RecyclerView.Adapter<MyMovieRecyclerViewAdapter.ViewHolder> implements View.OnClickListener {

    private  List<Movie> movies;
    private Context ctx;
    private View.OnClickListener listener;

    public MyMovieRecyclerViewAdapter(Context ctx) {
        this.ctx = ctx;
    }
    public void setData(List<Movie> data){
        this.movies = data;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_movie_list, parent, false);


        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        if (movies!=null){
            Movie movie = movies.get(position);
            holder.txtTitle.setText(movie.getTitle());
            movie.setVisible(false);
            holder.txtOriginalTitle.setText(movie.getOriginalTitle());
            holder.txtReleaseDate.setText("Lanzamiento: " + movie.getReleaseDate());
            if (NetworkUtils.isConnected(MyApp.getContext())){
                Glide.with(MyApp.getContext())
                        .load(ConstantsAPI.URL_IMAGE+movie.getPosterPath())
                        .into(holder.imgMovie);
            }else{
                holder.imgMovie.setImageDrawable(MyApp.getContext().getResources().getDrawable(R.drawable.ic_block));
            }

            holder.txtOverview.setText(movie.getOverview());
            holder.txtPopularity.setText("Popularidad: " + movie.getPopularity());
            holder.txtOverview.setVisibility(View.GONE);
            holder.cardMovie.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    holder.txtOverview.setVisibility(!movie.isVisible() ? View.VISIBLE : View.GONE);
                    movie.setVisible(!movie.isVisible());
                }
            });

        }
    }

    @Override
    public int getItemCount() {
        if (movies!=null){
            return movies.size();
        }else{
            return 0;
        }
    }

    @Override
    public void onClick(View view) {
        this.listener.onClick(view);
    }
    public void setOnclickListener(View.OnClickListener onClickListener){
        this.listener = onClickListener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private CardView cardMovie;
        private ImageView imgMovie;
        private TextView txtTitle, txtOriginalTitle, txtReleaseDate, txtPopularity, txtOverview;

        public ViewHolder(View view) {
            super(view);
            cardMovie = view.findViewById(R.id.cardMovie);
            imgMovie = view.findViewById(R.id.imgMovie);
            txtTitle = view.findViewById(R.id.txtTitle);
            txtOriginalTitle = view.findViewById(R.id.txtOriginalTitle);
            txtReleaseDate = view.findViewById(R.id.txtReleaseDate);
            txtPopularity = view.findViewById(R.id.txtPopularity);
            txtOverview = view.findViewById(R.id.txtOverview);


        }


    }
}