package com.andrea.examen.upax.data.local;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;

import com.andrea.examen.upax.data.entity.Movie;
import com.andrea.examen.upax.utils.constants.Constants;

@Database(entities = {Movie.class}, exportSchema = false, version = 1)
public abstract class RoomDatabase extends androidx.room.RoomDatabase {
   public static RoomDatabase instance;

   public abstract MovieDao getMovieDao();


   public static RoomDatabase getInstance(Context context) {
      if (instance == null) {
         instance = Room.databaseBuilder(context.getApplicationContext(), RoomDatabase.class, Constants.DATABASE)
                         .build();
      }
      return instance;
   }

}
