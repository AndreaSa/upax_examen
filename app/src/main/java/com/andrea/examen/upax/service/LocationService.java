package com.andrea.examen.upax.service;

import android.Manifest;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;

import com.andrea.examen.upax.app.MyApp;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static com.andrea.examen.upax.utils.DeviceUtils.getDeviceName;

public class LocationService extends Service implements LocationListener {
    protected LocationManager locationManager;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @Override
    public void onCreate() {
        super.onCreate();
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 10000, 0, this);
    }

    @Override
    public void onLocationChanged(@NonNull Location location) {
        Date date = Calendar.getInstance().getTime();
        //Log.d("onLocationChanged: ", location.getLatitude() + "");
        //Log.d("onLocationChanged1: ", date.toString());
        Map<String, Object> mark = new HashMap<>();
        mark.put("Device", getDeviceName());
        mark.put("latitude", location.getLatitude());
        mark.put("longitude", location.getLongitude());
        mark.put("time", date.toString());
        MyApp.getFirestoreDb().collection("Marks").add(mark).addOnSuccessListener(documentReference -> Log.d("added with ID: ", documentReference.getId())).addOnFailureListener(e -> Log.w("Error adding document", e));
    }


}
