package com.andrea.examen.upax.data;

import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.andrea.examen.upax.app.MyApp;
import com.andrea.examen.upax.data.entity.Movie;
import com.andrea.examen.upax.data.entity.PopularResponse;
import com.andrea.examen.upax.data.local.MovieDao;
import com.andrea.examen.upax.data.local.RoomDatabase;
import com.andrea.examen.upax.data.remote.ClientRetrofit;
import com.andrea.examen.upax.data.remote.MovieService;
import com.andrea.examen.upax.utils.NetworkUtils;
import com.andrea.examen.upax.utils.constants.ConstantsAPI;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MovieRepository {
    private final MovieService movieService;
    private final MovieDao movieDao;
    private MutableLiveData<List<Movie>> listMovies;


    public MovieRepository() {
        movieService = ClientRetrofit.getRetrofit().create(MovieService.class);
        movieDao = RoomDatabase.getInstance(MyApp.getContext()).getMovieDao();
    }

    public MutableLiveData<List<Movie>> getListMoviesFromAPI() {
        if (listMovies == null) {
            listMovies = new MutableLiveData<>();
        }

        if (NetworkUtils.isConnected(MyApp.getContext())) {
            movieService.getPopular(ConstantsAPI.API_KEY, ConstantsAPI.LANGUAGE_REQUEST).enqueue(new Callback<PopularResponse>() {
                @Override
                public void onResponse(Call<PopularResponse> call, Response<PopularResponse> response) {
                    if (response.isSuccessful()) {
                        listMovies.setValue(response.body().getResults());
                        //Insertar las Movies
                        insertMovies(response.body().getResults());
                    } else {
                        Toast.makeText(MyApp.getContext(), "Algo salió mal", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<PopularResponse> call, Throwable t) {
                    Toast.makeText(MyApp.getContext(), "Falló la conexión a Internet", Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            new getAll(movieDao).execute();
        }
        return listMovies;
    }


    public void insertMovies(List<Movie> movies) {
        new insertAsyncTask(movieDao).execute(movies);
    }

    class insertAsyncTask extends AsyncTask<List<Movie>, Void, Void> {
        private MovieDao movieDao;

        insertAsyncTask(MovieDao dao) {
            movieDao = dao;
        }

        @Override
        protected Void doInBackground(List<Movie>... lists) {
            movieDao.deleteAllMovies();
            movieDao.insertMovie(lists[0]);
            return null;
        }
    }

        class getAll extends AsyncTask<Void, Void, Void> {
            private MovieDao movieDao;
            private int size = 0;

            getAll(MovieDao dao) {
                movieDao = dao;
            }

            @Override
            protected Void doInBackground(Void... voids) {
                if (movieDao.getMovies().size()>0){
                    listMovies.postValue(movieDao.getMovies());
                    size = movieDao.getMovies().size();
                }else{
                    size = 0;
                    listMovies.postValue(new ArrayList<>());
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if (size==0){
                    Toast.makeText(MyApp.getContext(), "No hay datos descargados", Toast.LENGTH_SHORT).show();
                }

            }
        }





}
