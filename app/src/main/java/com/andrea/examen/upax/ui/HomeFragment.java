package com.andrea.examen.upax.ui;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.andrea.examen.upax.databinding.FragmentHomeBinding;


public class HomeFragment extends Fragment {


    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        FragmentHomeBinding fragmentHomeBinding = FragmentHomeBinding.inflate(inflater, container, false);
        View view = fragmentHomeBinding.getRoot();
        fragmentHomeBinding.gridMovies.setOnClickListener(view1 -> ((MainActivity) getActivity()).agregarFragmentos(1));
        fragmentHomeBinding.gridMap.setOnClickListener(v -> ((MainActivity) getActivity()).agregarFragmentos(2));
        fragmentHomeBinding.gridUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getActivity(), "Sección en desarrollo", Toast.LENGTH_SHORT).show();
            }
        });

        fragmentHomeBinding.gridContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialog();
            }
        });


        return view;
    }

    public void showDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("ISC. Andrea de Jesús Sánchez" +
                "\nTeléfono: 7151134076" +
                "\nEmail: andrea.tec@outlook.es" +
                "\nDomicilio: Zitácuaro, Michoacán.")
                .setTitle("Información");

        builder.setNegativeButton("Salir", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();

    }


}