package com.andrea.examen.upax.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.andrea.examen.upax.data.MovieRepository;
import com.andrea.examen.upax.data.entity.Movie;

import java.util.List;

public class MovieViewModel extends AndroidViewModel {
    private MovieRepository movieRepository;
    private LiveData<List<Movie>> listMovies;


    public MovieViewModel(@NonNull Application application) {
        super(application);
        movieRepository = new MovieRepository();


    }

    public LiveData<List<Movie>> getListMovies(){
        listMovies = movieRepository.getListMoviesFromAPI();
        return listMovies;
    }



}
