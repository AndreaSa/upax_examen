package com.andrea.examen.upax.data.local;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.andrea.examen.upax.data.entity.Movie;

import java.util.List;

@Dao
public interface MovieDao {
    @Query("SELECT * FROM movie")
    public List<Movie> getMovies();

    @Query("SELECT * FROM movie")
    public List<Movie> getMoviesNormal();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void insertMovie(List<Movie> movie);

    @Query("DELETE FROM movie")
    public void deleteAllMovies();

}
