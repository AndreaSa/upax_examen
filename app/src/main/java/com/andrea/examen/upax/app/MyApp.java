package com.andrea.examen.upax.app;

import android.app.Application;
import android.content.Context;

import com.google.firebase.firestore.FirebaseFirestore;

public class MyApp extends Application {
    private static MyApp instance;
    private static FirebaseFirestore db;

    public MyApp getInstance() {
        return instance;
    }

    public static FirebaseFirestore getFirestoreDb() {
        return db;
    }

    public static Context getContext() {
        return instance;
    }

    @Override
    public void onCreate() {
        instance = this;
        db = FirebaseFirestore.getInstance();
        super.onCreate();
    }
}
